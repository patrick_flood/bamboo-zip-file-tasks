package com.pronetbeans.bamboo.filezip;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.opensymphony.xwork.TextProvider;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Class for configuring parameters used by class for unzipping files.
 *
 * @author Adam Myatt
 */
public class FileExtractZipConfigurator extends AbstractTaskConfigurator {

    private TextProvider textProvider;

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition) {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put("propsZipLocation", params.getString("propsZipLocation"));
        config.put("propsExtractLocation", params.getString("propsExtractLocation"));
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context) {
        super.populateContextForCreate(context);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        context.put("propsZipLocation", taskDefinition.getConfiguration().get("propsZipLocation"));
        context.put("propsExtractLocation", taskDefinition.getConfiguration().get("propsExtractLocation"));

    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        context.put("propsZipLocation", taskDefinition.getConfiguration().get("propsZipLocation"));
        context.put("propsExtractLocation", taskDefinition.getConfiguration().get("propsExtractLocation"));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        final String propsZipLocation = params.getString("propsZipLocation");
        if (StringUtils.isEmpty(propsZipLocation)) {
            errorCollection.addError("propsZipLocation", textProvider.getText("com.pronetbeans.bamboo.filezip.FileCreateZipTask.error"));
        }
    }

    public void setTextProvider(final TextProvider textProvider) {
        this.textProvider = textProvider;
    }
}
